package com.c4app.connectfour.server;

import com.c4app.connectfour.util.C4PacketBuilder;
import com.c4app.connectfour.util.*;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * The C4ServerGame is responsible for receiving moves from the client and
 * responding with a move (which is chosen by the AI)
 * 
 * AI chooses a move based on the following logic. If there is a move that 
 * leads directly win, it choses this move. If there is a move that will block 
 * a client from winning, it will choose this move. If there are no moves that
 * match the above conditions, it will choose a column randomly.
 *
 * @author Nick
 */
public class C4ServerGame {
    private int[][] board;
    private int totalColumns = 7;
    private int totalRows = 6;
    private final static Random RANDOM = new Random();
    private SocketManager socketManager;
    private Socket socket;

    public C4ServerGame(Socket socket) {
        board = new int[totalColumns][];
        for (int col = 0; col < board.length; col++) {
            board[col] = new int[totalRows];
            for (int row = 0; row < board[col].length; row++) {
                board[col][row] = 0;
                // 1 = player, 2 = AI
            }
        }

        this.socket = socket;
        socketManager = new SocketManager(socket);
        setupReplyPacketActions();
    }

    /**
     * Executes the C4ServerGame
     */
    public void execute() {
        try {
            socketManager.waitForReply();
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    /**
     * This method is used to respond to a move received from the client.
     *
     * The method checks if the client has won. If yes, it notifies the client.
     * If not, it makes the AI play a move. Then it checks if the move played by the
     * AI leads to a lose or a tie and notifies the client.
     *
     * @param column
     */
    private void acceptMoveFromClient(int column) {
        try {
            int availableRow = computeAvailableRow(column);
            boolean isPlayerWon = isWinningMove(column, availableRow, 1);
            playMove(column, availableRow, 1);
        
        if (isPlayerWon) {
            byte[] packetToSend = C4PacketBuilder.clientWonGame();
            socketManager.sendPacket(packetToSend, false);
            System.out.println(socket.getInetAddress().getHostAddress() + " has won the game");
        } else {
            int columnPlayedByAI = getColumnFromAI();
            int rowPlayedByAI = computeAvailableRow(columnPlayedByAI);
            boolean isServerWon = isWinningMove(columnPlayedByAI, rowPlayedByAI, 2);
            playMove(columnPlayedByAI, rowPlayedByAI, 2);
            boolean isGameTie = isTie();
                    
            if (isServerWon) {
                byte[] packetToSend = C4PacketBuilder.serverMovedClientLost(columnPlayedByAI);
                socketManager.sendPacket(packetToSend, false);
                System.out.println(socket.getInetAddress().getHostAddress() + " lost the game");
            } else if (isGameTie) {
                byte[] packetToSend = C4PacketBuilder.serverMovedGameTie(columnPlayedByAI);
                socketManager.sendPacket(packetToSend, false);
                System.out.println(socket.getInetAddress().getHostAddress() + " the game ended in a tie");
            } else {
                byte[] packetToSend = C4PacketBuilder.serverMoved(columnPlayedByAI);
                socketManager.sendPacket(packetToSend, true);
                
            }
        
    }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    /**
     * Helper method to play a move in the board.
     * Simply puts the playerInt into the specified coordinates.
     *
     * @param column The column to play
     * @param row The row to play
     * @param playerInt The integer that represents the player (1 -> client, 2 -> server)
     */
    private void playMove(int column, int row, int playerInt) {
        board[column][row] = playerInt;
    }

    /**
     * This method computes a logical move to play
     * @return The move that has been computed by the AI
     */
    private int getColumnFromAI() {
        int chosenColumn = -1;
        
         List<Integer> winningMoves = new ArrayList<>();
         List<Integer> blockingMoves = new ArrayList<>();
         List<Integer> ordinaryMoves = new ArrayList<>();
         
         for (int col = 0; col < board.length; col++) {
             if (!isColumnFull(col)) {
                 int availableRow = computeAvailableRow(col);
                 
                 if (isWinningMove(col, availableRow, 2)) {
                     winningMoves.add(col);
                 } else if (isWinningMove(col, availableRow, 1)) {
                     blockingMoves.add(col);
                 } else {
                     ordinaryMoves.add(col);
                 }
             }
         }
         
         if (!winningMoves.isEmpty()) {
             chosenColumn = winningMoves.get(RANDOM.nextInt(winningMoves.size()));
         } else if (!blockingMoves.isEmpty()) {
             chosenColumn = blockingMoves.get(RANDOM.nextInt(blockingMoves.size()));
         } else if (!ordinaryMoves.isEmpty()) {
             chosenColumn = ordinaryMoves.get(RANDOM.nextInt(ordinaryMoves.size()));
         }
         
         return chosenColumn;
    }

    /**
     * Helper method to verify if a move will be a winning move
     *
     * @param moveCol The column
     * @param moveRow The row
     * @param playerInt The integer that represents the player (1 -> client, 2 -> server)
     * @return True if the move will be a winning move
     */
    private boolean isWinningMove(int moveCol, int moveRow, int playerInt)  {
        boolean isWinningMove = false;
        
        // Copying array so we'll not modifying the original array
        // when checking if the move will be a winning move
        int[][] boardCopy = Arrays.stream(board)
                .map(column -> Arrays.copyOf(column, column.length))
                .toArray(int[][]::new);
                     
        boardCopy[moveCol][moveRow] = playerInt;
        
        int[] columnArray = boardCopy[moveCol];

        int[] rowArray = new int[totalColumns];
        for (int col = 0; col < totalColumns; col++) {
            rowArray[col] = boardCopy[col][moveRow];
        }

        int[] diagonalArray1 = new int[6];
        int[] diagonalArray2 = new int[6];


        // Creating diagonal arrays which will contain the diagonals of the C4 game
        // that contain this specified moveCol and moveRow.
        // Finding the start row for every diagonal so it will be easier to get the diagonal
        int startRowDiagonal1 = (moveRow - moveCol);
        int startRowDiagonal2 = (moveRow + moveCol);

        // Diagonal 1, increments col, increments row
        for (int i = 0, col = 0, row = startRowDiagonal1; col < totalColumns || row < totalRows; i++, col++, row++) {
            if (col < 0 || row < 0 || col >= totalColumns || row >= totalRows) {
                i--;
                continue;
            }
            diagonalArray1[i] = boardCopy[col][row];
        }

        // Diagonal 2, increments col, decrements row
        for (int i = 0, col = 0, row = startRowDiagonal2; col < totalColumns || row >= 0; i++, col++, row--) {
            if (col < 0 || row < 0 || col >= totalColumns || row >= totalRows) {
                i--;
                continue;
            }
            diagonalArray2[i] = boardCopy[col][row];
        }

        if (containsConsecutive(4, playerInt, columnArray))
            isWinningMove = true;
        else if (containsConsecutive(4, playerInt, rowArray))
            isWinningMove = true;
        else if (containsConsecutive(4, playerInt, diagonalArray1))
            isWinningMove = true;
        else 
            isWinningMove = containsConsecutive(4, playerInt, diagonalArray2);
        
        return isWinningMove;
    }

    /**
     * Helper method to verify if there is a tie (the whole board is full)
     * @return True if there is a tie
     */
    private boolean isTie() {
        for (int[] col: board) {
            for (int row: col) {
                if (row == 0)
                    return false;
            }
        }
        return true;
    }

    /**
     * Helper method used to verify if an array has consecutive integers
     * The neededConsecutiveInts specifies how many consecutive integers to look for
     *
     * @param neededConsecutiveInts How many consecutive integers to look for
     * @param integer The integer to look for
     * @param integerArray The integer array to analyze
     * @return True if there are consecutive integers in the array
     *        (The neededConsecutiveInts specifies how many consecutive integers to look for)
     */
    private boolean containsConsecutive(int neededConsecutiveInts, int integer, int[] integerArray) {
        int consecutiveInts = 0;
        int index = 0;
        while (index < integerArray.length) {
            if (integerArray[index] == integer) {
                consecutiveInts++;
            } else {
                consecutiveInts = 0;
            }

            if (consecutiveInts == neededConsecutiveInts) {
                return true;
            }

            index++;
        }
        return false;
    }

    /**
     * A helper method used too verify if a column is full
     *
     * @param col The column to verify
     * @return True if the column is full
     */
    private boolean isColumnFull(int col) {
        if (board[col][0] == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Helper method to determine the available row for a column.
     * In other words, it returns the row where a marker will be put
     * if a maker would have been placed in that column.
     *
     * @param column The column used to calculate the row where the marker will be put
     * @return The row where a marker will be put if a maker
     *          would have been placed in that column.
     */
    private int computeAvailableRow(int column) {
        int lastRowIndex = board[column].length - 1;

        for (int row = lastRowIndex; row >= 0; row--) {
            if (board[column][row] == 0) {
                return row;
            }
        }

        return -1;
    }

    /**
     * Method which ends the game if a manual end game was requested by the
     * client.
     */
    private void manualEndGame() {
        try {
            byte[] packetToSend = C4PacketBuilder.gameEndedByClient();
            socketManager.sendPacket(packetToSend, false);
            System.out.println(socket.getInetAddress().getHostAddress() + " ended the game manually");
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    /**
     * Sets the callbacks for all the Op Codes that might be received from the client.
     */
    private void setupReplyPacketActions() {
        socketManager.setReplyOpCodeHandler(C4PacketBuilder.CLIENT_PLAY_MOVE,
                (data) -> {
                    acceptMoveFromClient(data);
                });

        socketManager.setReplyOpCodeHandler(C4PacketBuilder.REQUEST_END_GAME,
                (data) -> {
                    manualEndGame();
                });

    }
}