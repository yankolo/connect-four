package com.c4app.connectfour.server;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The C4ServerApp is the server application that establishes a connection with a client.
 * The server application runs forever, accepting and servicing connections
 * (Servicing multiple clients, BUT only one at a time)
 *   
 * @author Thomas
 *
 */
public class C4ServerApp {
    public static void main(String[] args) {
        C4ServerApp serverApp = new C4ServerApp();
        serverApp.execute();
    }

    /**
     * Executes the server application
     */
	public void execute() {
	    try {
            ServerSocket serverSocket = new ServerSocket(50000);
            System.out.println("The IP of server is: " + Inet4Address.getLocalHost().getHostAddress());

            // Infinite loop which waits for a client to connect. Start a session when
            // a client has connected.
            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println(clientSocket.getInetAddress().getHostAddress() +  " has connected");
                C4ServerSession serverSession = new C4ServerSession(clientSocket);
                serverSession.execute();
            }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
	}
}
