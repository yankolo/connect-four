/**
 * 
 */
package com.c4app.connectfour.server;

import com.c4app.connectfour.util.C4PacketBuilder;
import com.c4app.connectfour.util.SocketManager;
import java.io.IOException;
import java.net.Socket;

/**
 * The C4ServerSession is responsible for creating new C4ServerGames when requested by
 * the client.
 *
 * @author Thomas
 */
public class C4ServerSession {
    private Socket socket;
    private SocketManager socketManager;
    
    public C4ServerSession(Socket socket) {
        this.socket = socket;
        this.socketManager = new SocketManager(socket);
        
        setupReplyOpCodeActions();
    }

    /**
     * Executes the server session (waits for packets from the client)
     */
    public void execute() {
        try {
            socketManager.waitForReply();
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

    /**
     * Starts a new C4ServerGame. Executed when a packet is received from the client
     * that requests a new game.
     */
    private void startGame() {
         try {
             byte[] packetToSend = C4PacketBuilder.gameStartedByServer();
             socketManager.sendPacket(packetToSend, false);

             System.out.println(socket.getInetAddress().getHostAddress() + " started a game");
             C4ServerGame serverGame = new C4ServerGame(socket);
             serverGame.execute();
         } catch (IOException ioe) {
             System.out.println(ioe.getMessage());
         }
    }

    /**
     * Ends the current session. Executed when a packet is received from the client
     * that requests to end the session.
     */
    private void endSession() {
        try {
            byte[] packetToSend = C4PacketBuilder.sessionWillEnd();
            socketManager.sendPacket(packetToSend, false);
            
             if (socket != null) {
                socket.close();
            }
            System.out.println(socket.getInetAddress().getHostAddress() + " ended the session");
         } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
         }
    }

    /**
     * Sets the callbacks for all the Op Codes that might be received from the client.
     */
    private void setupReplyOpCodeActions() {
        socketManager.setReplyOpCodeHandler(C4PacketBuilder.REQUEST_GAME_START,
                (data) -> {
                    startGame();
                    try {
                        socketManager.waitForReply();
                    } catch (IOException ioe) {
                        System.out.println(ioe.getMessage());
                    }
                });

        socketManager.setReplyOpCodeHandler(C4PacketBuilder.REQUEST_END_SESSION,
                (data) -> {
                    endSession();
                });
    }
}
