package com.c4app.connectfour.client.gui;

import com.c4app.connectfour.client.business.C4ClientApp;
import com.c4app.connectfour.client.business.C4ClientSession;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * The AppWindowController provides a graphical user interface to a C4ClientApp.
 * It is the window that displays the field that asks the user to enter an IP.
 *
 * It is required to set the StageManager and the C4ClientApp for the controller to work.
 *
 * @author Yanik
 */
public class AppWindowController {
    private StageManager stageManager;
    private C4ClientApp c4ClientApp;
    private int defaultPort = 50000;

    @FXML
    private TextField hostNameField;
    @FXML
    private TextField portField;
    @FXML
    private Button connectButton;

    /**
     * The initialize method initializes the window. In this case, it sets the
     * port # into the port field and sets the events handlers for the window.
     */
    @FXML
    public void initialize() {
        portField.setText(String.valueOf(defaultPort));

        // Setup event handler for host name field (enter pressed)
        hostNameField.setOnAction(
                (actionEvent) -> {
                    c4ClientApp.connectToServer(hostNameField.getText(), defaultPort);
                }
        );


        // Setup event handler for the connect button
        connectButton.setOnAction(
                (actionEvent) -> {
                    c4ClientApp.connectToServer(hostNameField.getText(), defaultPort);
                }
        );
    }

    /**
     * This method loads the session window. It is needed to pass a C4ClientSession
     * which the session window will use to configure itself (e.g. setup the event handlers)
     * @param c4ClientSession A C4ClientSession which the session window will use to configure
     *                       itself (e.g. setup the event handlers)
     */
    private void loadSessionWindow(C4ClientSession c4ClientSession) {
        try {
            // Loading the start window from a FXML file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/client-gui/session-window.fxml"));
            Parent window = loader.load();
            SessionWindowController sessionWindowController = loader.getController();
            sessionWindowController.setStageManager(stageManager);
            sessionWindowController.setC4ClientSession(c4ClientSession);

            // Setting up the scene and showing the stage
            Stage sessionStage = new Stage();
            Scene scene = new Scene(window);
            sessionStage.setScene(scene);
            stageManager.showStage(sessionStage);
        } catch (IOException e) {
            // Unrecoverable error
            throw new RuntimeException(e);
        }
    }

    /**
     * This method is used to display an exception (in user friendly way) which
     * can occur when the client couldn't connect to the server.
     * @param e The exception that was thrown
     */
    private void displayConnectionError(Exception e) {
        String errorMessage = null;

        if (e instanceof UnknownHostException) {
            errorMessage = "Unknown host";
        } else if (e instanceof IOException) {
            errorMessage = "Error when trying to connect. Please verify all input is correct and retry.";
        } else {
            errorMessage = "Unknown error. Please contact software manufacturer.";
        }

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Connection Error");
        alert.setContentText(errorMessage);

        alert.showAndWait();

    }

    /**
     * Sets the StageManager that will be used to display/close Stages (windows)
     * It is required to set the StageManager for the controller to work.
     * @param stageManager The StageManager that will be used to display/close Stages (windows)
     */
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    /**
     * Sets the C4ClientApp object that will be used by the window.
     * In addition, it initializes all the necessary callbacks in the C4ClientApp.
     *
     * It is required to set the C4ClientApp for the controller to work.
     * @param c4ClientApp The C4ClientApp object that will be used by the window.
     */
    public void setC4ClientApp(C4ClientApp c4ClientApp) {
        this.c4ClientApp = c4ClientApp;

        c4ClientApp.setOnConnectionError(this::displayConnectionError);
        c4ClientApp.setOnConnected(this::loadSessionWindow);
    }
}
