package com.c4app.connectfour.client.gui;

import com.c4app.connectfour.client.business.C4ClientGame;
import com.c4app.connectfour.client.business.C4ClientSession;
import com.c4app.connectfour.util.Action;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * The SessionWindowController provides a graphical user interface to 
 * interfacea with a C4ClientSession.
 * 
 * The window displays a menu with two options: "Start a new game" 
 * and "End session".
 *
 * @author Yanik
 */
public class SessionWindowController {
    private C4ClientSession c4ClientSession;
    private StageManager stageManager;
    private Action onPlayButtonClick;
    private Action onEndSessionButtonClick;

    @FXML
    private Button playButton;
    @FXML
    private Button endSessionButton;

    /**
     * The initialize method initializes the window. In this case, it initializes
     * the events handlers of the buttons in the session menu.
     */
    @FXML
    public void initialize() {
        playButton.setOnAction(
                (actionEvent) -> {
                    c4ClientSession.requestStartGame();
                }
        );

        endSessionButton.setOnAction(
                (actionEvent) -> {
                    c4ClientSession.requestEndSession();
                }
        );
    }

    /**
     * This method loads the game window. It is needed to pass a C4ClientGame
     * which the game window will use to configure itself (e.g. setup the event handlers)
     * @param c4ClientGame A C4ClientGame which the game window will use to configure
     *                       itself (e.g. setup the event handlers)
     */
    private void loadGameWindow(C4ClientGame c4ClientGame) {
        try {
            // Loading the main window from a FXML file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/client-gui/main-window.fxml"));
            Parent window = loader.load();
            GameWindowController gameWindowController = loader.getController();
            gameWindowController.setStageManager(stageManager);
            gameWindowController.setC4ClientGame(c4ClientGame);

            // Setting up the scene and showing the stage
            Stage gameStage = new Stage();
            Scene scene = new Scene(window);
            gameStage.setScene(scene);
            stageManager.showStage(gameStage);
        } catch (IOException e) {
            // Unrecoverable error
            throw new RuntimeException(e);
        }
    }

    /**
     * Displays a message when there is a communication error with the server
     */
    private void showCommunicationError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Communication Error");
        alert.setContentText("An error occurred while trying to communicate with the server. " +
                "The server might have shutdown or you don't have internet connection. " +
                "If the error persists, please contact the software manufacturer.");

        alert.showAndWait();

        stageManager.backToFirstStage();
    }

    /**
     * Sets the StageManager that will be used to display/close Stages (windows)
     * It is required to set the StageManager for the controller to work.
     * @param stageManager The StageManager that will be used to display/close Stages (windows)
     */
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    /**
     * Sets the C4ClientSession object that will be used by the window.
     * In addition, it initializes all the necessary callbacks in the C4ClientSession.
     *
     * It is required to set the C4ClientSession for the controller to work.
     * @param c4ClientSession The C4ClientSession object that will be used by the window.
     */
    public void setC4ClientSession(C4ClientSession c4ClientSession) {
        this.c4ClientSession = c4ClientSession;

        c4ClientSession.setOnGameStarted(this::loadGameWindow);
        c4ClientSession.setOnSessionEnded(stageManager::closeCurrentStage);
        c4ClientSession.setOnCommunicationError(this::showCommunicationError);
    }
}
