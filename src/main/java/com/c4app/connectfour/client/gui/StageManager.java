package com.c4app.connectfour.client.gui;

import javafx.stage.Stage;
import java.util.Stack;

/**
 * The StageManager provides a centralized way to easily open and close windows.
 * When a window is opened, the previous window is closed; and when a windows is closed,
 * the previous window is displayed. The StageManager achieves this using a Stack. The
 * topmost Stage (window) in the stack is displayed. Since the StageManager keeps track of
 * all the stages, only one instance should be used in the GUI application.
 *
 * The reason this class is created is that the Connect Four client GUI requires
 * the use of multiple windows. However, for a better experience, whenever a window opens,
 * the previous window should close. The same applies when closing windows, as when a windows
 * is closed the previously open window should open.
 *
 * For instance: after establishing a connection with the server, a session menu
 * appears and presents two options (start a new game or end the session) opens 
 * and the connection window closes.
 * 
 * In the menu, after choosing to end the session, this the menu closes and the connection
 * window opens.
 *
 * @author Yanik
 */
public class StageManager {
    private Stack<Stage> stageStack;

    public StageManager() {
        stageStack = new Stack<>();
    }

    /**
     * Closes the Stage that is on top of the Stack (if there is one), puts the passed
     * Stage in the Stack and displays it.
     * @param stage The Stage to display
     */
    public void showStage(Stage stage) {
        if (!stageStack.empty())
            stageStack.peek().close();

        stageStack.push(stage);
        stage.show();
    }

    /**
     * Closes the Stage that is on top of the Stack and removes it from the Stack.
     */
    public void closeCurrentStage() {
        Stage currentStage = stageStack.pop();
        currentStage.close();

        if (!stageStack.empty())
            stageStack.peek().show();
    }

    /**
     * Closes and removes all the Stages in the Stack but the first one.
     */
    public void backToFirstStage() {
        stageStack.peek().close();

        Stage firstStage = null;

        while (!stageStack.empty()) {
            firstStage = stageStack.pop();
        }

        stageStack.push(firstStage).show();
    }
}
