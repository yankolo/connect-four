package com.c4app.connectfour.client.gui;

import com.c4app.connectfour.client.business.C4ClientGame;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import static com.c4app.connectfour.client.business.C4ClientGame.GameState;

/**
 * The GameWindowController provides a graphical user interface to a C4ClientGame.
 * It is the window that displays the Connect Four game board.
 *
 * @author Yanik
 */
public class GameWindowController {
    private StageManager stageManager;
    private C4ClientGame c4ClientGame;

    @FXML
    private HBox mainHBox;
    @FXML
    private Label clientMarkersLeft;
    @FXML
    private Label serverMarkersLeft;
    @FXML
    private Button exitGameButton;

    private Lighting defaultColumnLighting;
    private Lighting hoverColumnLighting;

    private EventHandler<MouseEvent> onColumnClickHandler;
    private EventHandler<MouseEvent> onColumnEnteredHandler;
    private EventHandler<MouseEvent> onColumnExitedHandler;

    /**
     * The initialize method initializes the window. In this case, it initializes
     * the column lighting and initializes all the necessary event handlers.
     */
    @FXML
    public void initialize() {
        exitGameButton.setOnAction(
                (actionEvent) -> {
                    c4ClientGame.requestEndGame();
                }
        );

        initializeDefaultLighting();
        initializeHoverLighting();

        // Presetting the lighting for all columns
        for (Node column: mainHBox.getChildren()) {
            column.setEffect(defaultColumnLighting);
        }

        // Creating all the necessary event handlers for the columns
        // That way we can easily remove the event handlers when needed

        onColumnClickHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                VBox column = (VBox) event.getSource();

                int columnIndex = mainHBox.getChildren().indexOf(column);

                c4ClientGame.clientPlayMove(columnIndex);
            }
        };

        onColumnEnteredHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                VBox column = (VBox) event.getSource();
                column.setEffect(hoverColumnLighting);
            }
        };

        onColumnExitedHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                VBox column = (VBox) event.getSource();
                column.setEffect(defaultColumnLighting);
            }
        };

        // Setting up mouse events for all columns
        for (Node column: mainHBox.getChildren()) {
            column.addEventHandler(MouseEvent.MOUSE_PRESSED, onColumnClickHandler);
            column.addEventHandler(MouseEvent.MOUSE_ENTERED, onColumnEnteredHandler);
            column.addEventHandler(MouseEvent.MOUSE_EXITED, onColumnExitedHandler);
        }

    }

    /**
     * Places a marker into the specified coordinates
     *
     * @param col The column
     * @param row The row
     */
    private void placeMarker(int col, int row) {
        int player = -1;

        if (c4ClientGame.getGameState() == GameState.CLIENT_PLAYS)
            player = 1;
        else if (c4ClientGame.getGameState() == GameState.SERVER_PLAYS)
            player = 2;

        VBox selectedColumn = (VBox) mainHBox.getChildren().get(col);
        Node selectedCell = selectedColumn.getChildren().get(row);

        String applicableStyleClass = "";
        if (player == 1)
            applicableStyleClass = "circle-client";
        else if (player == 2)
            applicableStyleClass = "circle-server";

        selectedCell.getStyleClass().add(applicableStyleClass);
    }

    /**
     * Removes hover effect of a column and all associated
     * event handlers. This functionality is necessary when a column
     * becomes full (so that we will not send invalid requests)
     *
     * @param column The column that is full
     */
    private void updateFullColumn(int column) {
        VBox columnThatIsFull = (VBox) mainHBox.getChildren().get(column);

        columnThatIsFull.setEffect(defaultColumnLighting);
        columnThatIsFull.getStyleClass().remove("column-clickable");

        columnThatIsFull.removeEventHandler(MouseEvent.MOUSE_PRESSED, onColumnClickHandler);
        columnThatIsFull.removeEventHandler(MouseEvent.MOUSE_ENTERED, onColumnEnteredHandler);
        columnThatIsFull.removeEventHandler(MouseEvent.MOUSE_EXITED, onColumnExitedHandler);
    }

    /**
     * Updates the client and server marker counts.
     *
     * @param clientMarkerCount The client marker count
     * @param serverMarkerCount The server marker count
     */
    private void updateMarkersCount(int clientMarkerCount, int serverMarkerCount) {
        clientMarkersLeft.setText(String.valueOf(clientMarkerCount));
        serverMarkersLeft.setText(String.valueOf(serverMarkerCount));
    }

    /**
     * Changes the window depending on the GameStage passed.
     * For example, if the GameState is CLIENT_WON, a win message will be
     * displayed.
     *
     * @param gameState The new GameStage of the C4ClientGame
     */
    private void updateGameState(GameState gameState) {
        if (gameState == GameState.CLIENT_WON) {
            displayWinMessage();
            stageManager.closeCurrentStage();
        } else if (gameState == GameState.CLIENT_LOST) {
            displayLostMessage();
            stageManager.closeCurrentStage();
        } else if (gameState == GameState.GAME_TIE) {
            displayTieMessage();
            stageManager.closeCurrentStage();
        } else if (gameState == GameState.GAME_ENDED_MANUALLY) {
            displayManualEndMessage();
            stageManager.closeCurrentStage();
        }
    }

    /**
     * Displays a message that the client has won the game
     */
    private void displayWinMessage() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Game Ended");
        alert.setHeaderText("You have won!");
        alert.setContentText("You have won the game. Press OK to return to the menu.");

        alert.showAndWait();
    }

    /**
     * Displays a message that the client has lost the game
     */
    private void displayLostMessage() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Game Ended");
        alert.setHeaderText("You have Lost :(");
        alert.setContentText("You have lost the game :( Press OK to return to the menu.");

        alert.showAndWait();
    }

    /**
     * Displays a message that the the game has ended with a tie
     */
    private void displayTieMessage() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Game Ended");
        alert.setHeaderText("It is a tie");
        alert.setContentText("The game has ended with a tie. Press OK to return to the menu.");

        alert.showAndWait();
    }

    /**
     * Displays a message that the user has manually ended the game (top right "end game" button)
     */
    private void displayManualEndMessage() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Game Ended");
        alert.setHeaderText("Game Ended");
        alert.setContentText("You have manually ended the game. Press OK to return to the menu.");

        alert.showAndWait();
    }

    /**
     * Displays a message when there is a communication error with the server
     */
    private void displayCommunicationError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Communication Error");
        alert.setContentText("An error occurred while trying to communicate with the server. " +
                "The server might have shutdown or you don't have internet connection. " +
                "If the error persists, please contact the software manufacturer.");

        alert.showAndWait();

        stageManager.backToFirstStage();
    }

    /**
     * Initializes the default lighting that will be used on
     * columns that are not hovered. (Lighting gives a special effect on JavaFX
     * elements as if real light is present)
     */
    private void initializeDefaultLighting() {
        Light.Distant light = new Light.Distant();

        light.setAzimuth(45.0);
        light.setElevation(60.0);

        Lighting lighting = new Lighting(light);
        lighting.setSurfaceScale(5.0);

        lighting.setLight(light);

        defaultColumnLighting = lighting;
    }

    /**
     * Initializes the lighting that will be used on columns that are hovered.
     * (Lighting gives a special effect on JavaFX elements as if real light is present)
     */
    private void initializeHoverLighting() {
        Light.Distant light = new Light.Distant();

        light.setAzimuth(45.0);
        light.setElevation(45.0);

        Lighting lighting = new Lighting(light);
        lighting.setSurfaceScale(5.0);
;
        lighting.setLight(light);

        hoverColumnLighting = lighting;
    }

    /**
     * Sets the StageManager that will be used to display/close Stages (windows)
     * It is required to set the StageManager for the controller to work.
     * @param stageManager The StageManager that will be used to display/close Stages (windows)
     */
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    /**
     * Sets the C4ClientGame object that will be used by the window.
     * In addition, it initializes all the necessary callbacks in the C4ClientGame.
     *
     * It is required to set the C4ClientGame for the controller to work.
     * @param c4ClientGame The C4ClientApp object that will be used by the window.
     */
    public void setC4ClientGame(C4ClientGame c4ClientGame) {
        this.c4ClientGame = c4ClientGame;

        c4ClientGame.setOnMarkerCountChange(this::updateMarkersCount);
        c4ClientGame.setOnGameStateChange(this::updateGameState);
        c4ClientGame.setOnColumnFull(this::updateFullColumn);
        c4ClientGame.setOnCommunicationError(this::displayCommunicationError);
        c4ClientGame.setOnMovePlayed(this::placeMarker);
    }
}
