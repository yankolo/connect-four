package com.c4app.connectfour.client;


import com.c4app.connectfour.client.business.C4ClientApp;
import com.c4app.connectfour.client.gui.AppWindowController;
import com.c4app.connectfour.client.gui.StageManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Main class for client side gui. This is the first class run 
 * for the client application.
 *
 * @author Yanik
 */
public class ClientGUIApplication extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * This overridden start() method loads the the AppWindowController, which is responsible
     * to display the C4ClientApp.
     */
    @Override
    public void start(Stage primaryStage) {
        try{
            StageManager stageManager = new StageManager();

            // Creating business class
            C4ClientApp c4ClientApp = new C4ClientApp();

            // Loading the start window from a FXML file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/client-gui/start-window.fxml"));
            Parent window = loader.load();
            AppWindowController appWindowController = loader.getController();
            appWindowController.setStageManager(stageManager);
            appWindowController.setC4ClientApp(c4ClientApp);

            // Setting up the scene and showing the stage
            Scene scene = new Scene(window);
            primaryStage.setScene(scene);
            stageManager.showStage(primaryStage);
        } catch (IOException e) {
            // Unrecoverable error
            throw new RuntimeException(e);
        }
    }
}
