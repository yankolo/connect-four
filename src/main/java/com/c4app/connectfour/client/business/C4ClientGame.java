package com.c4app.connectfour.client.business;

import com.c4app.connectfour.util.Action;
import com.c4app.connectfour.util.C4PacketBuilder;
import com.c4app.connectfour.util.SocketManager;

import java.io.IOException;
import java.net.Socket;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * The C4ClientGame is responsible to play the game on the client side.
 * When the user makes a move, it sends a packet to the server that contains the
 * column that the user has played.
 *
 * To decouple this class from the GUI, the C4ClientGame executes callbacks
 * when different things happen.
 *
 * Description of each callbacks:
 * 
 *      onGameStateChange -> executes when the state of the GameState of the game
 *                           changes. The GameState is passed to the callback.
 *      onMovePlayed -> executes when a move is played by the player.
 *                      The column and row of the move is passed to the callback.
 *      onMarkerCountChange -> executes when the marker count for the client or
 *                             server changes. Both marker counts are passed to the
 *                             callback.
 *      onColumnFull -> executes when a column becomes full. The column # is passed
 *                      to the callback.
 *      onCommunicationError -> executes when there is problem to communicate with the
 *                              server.
 *
 * The callbacks of the C4ClientGame need to be set for it to work properly.
 *
 * @author Yanik
 */
public class C4ClientGame {
    public enum GameState {
        CLIENT_PLAYS,
        SERVER_PLAYS,
        CLIENT_WON,
        CLIENT_LOST,
        GAME_TIE,
        GAME_ENDED_MANUALLY
    }

    private GameState gameState;
    private Consumer<GameState> onGameStateChange;
    private BiConsumer<Integer, Integer> onMovePlayed;
    private BiConsumer<Integer, Integer> onMarkerCountChange;
    private Consumer<Integer> onColumnFull;
    private Action onCommunicationError;

    private SocketManager socketManager;

    private int[][] board;
    private int clientMarkerCount;
    private int serverMarkerCount;

    public C4ClientGame(Socket socket) {
        board = new int[7][];
        for (int col = 0; col < board.length; col++) {
            board[col] = new int[6];
            for (int row = 0; row < board[col].length; row++) {
                board[col][row] = 0;
            }
        }

        this.clientMarkerCount = 21;
        this.serverMarkerCount = 21;

        this.socketManager = new SocketManager(socket);

        setupReplyPacketActions();
    }

    /**
     * Sends a REQUEST_END_GAME to the server.
     */
    public void requestEndGame() {
        try {
            byte[] packetToSend = C4PacketBuilder.requestEndGame();
            socketManager.sendPacket(packetToSend, true);
        } catch (IOException e) {
            onCommunicationError.execute();
        }
    }

    /**
     * Plays the chosen column by the client. First it updates the
     * C4ClientGame to represent the played move. Then it sends the
     * played move (CLIENT_PLAY_MOVE) to the server.
     * @param column The column play by the client
     */
    public void clientPlayMove(int column) {
        changeGameState(GameState.CLIENT_PLAYS);
        playMove(column, 1);

        try {
            changeGameState(GameState.SERVER_PLAYS);
            byte[] packetToSend = C4PacketBuilder.clientPlayMove(column);
            socketManager.sendPacket(packetToSend, true);
        } catch (IOException e) {
            onCommunicationError.execute();
        }
    }

    /**
     * Helper method to change the the GameState of the game
     * @param gameState The new GameState of the game
     */
    private void changeGameState(GameState gameState) {
        this.gameState = gameState;
        onGameStateChange.accept(gameState);
    }

    /**
     * Method used to update the C4ClientGame to represent the a move
     * that has been played by the specified player.
     * Example, if playMove(1, 1) is called, the C4ClientGame will be updated
     * in such a way that a marker will be put in the first column by player 1 (client)
     *
     * Player 1 -> Client
     * Player 2 -> Server
     * @param column The column in which a marker will be put
     * @param player The player that puts the marker (1 -> client, 2 -> server)
     */
    private void playMove(int column, int player) {
        int availableRow = computeAvailableRow(column);
        board[column][availableRow] = player;
        boolean isColumnFull = availableRow == 0;

        onMovePlayed.accept(column, availableRow);
        if (isColumnFull)
            onColumnFull.accept(column);

        if (player == 1)
            clientMarkerCount--;
        else if (player == 2)
            serverMarkerCount--;

        onMarkerCountChange.accept(clientMarkerCount, serverMarkerCount);
    }

    /**
     * Helper method to determine the available row for a column.
     * In other words, it returns the row where a marker will be put
     * if a maker would have been placed in that column.
     * @param column The column used to calculate the row where the marker will be put
     * @return The row where a marker will be put if a maker
     *          would have been placed in that column.
     */
    private int computeAvailableRow(int column) {
        int lastRowIndex = board[column].length - 1;

        for (int row = lastRowIndex; row >= 0; row--) {
            if (board[column][row] == 0) {
                return row;
            }
        }

        return -1;
    }

    /**
     * Sets the callbacks for all the Op Codes that might be received from the server.
     */
    private void setupReplyPacketActions() {
        socketManager.setReplyOpCodeHandler(C4PacketBuilder.SERVER_MOVED,
                (data) -> {
                    playMove(data, 2);
                });

        socketManager.setReplyOpCodeHandler(C4PacketBuilder.CLIENT_WON_GAME,
                (data) -> {
                    changeGameState(GameState.CLIENT_WON);
                });

        socketManager.setReplyOpCodeHandler(C4PacketBuilder.SERVER_MOVED_CLIENT_LOST,
                (data) -> {
                    playMove(data, 2);
                    changeGameState(GameState.CLIENT_LOST);
                });

        socketManager.setReplyOpCodeHandler(C4PacketBuilder.SERVER_MOVED_GAME_TIE,
                (data) -> {
                    playMove(data, 2);
                    changeGameState(GameState.GAME_TIE);
                });

        socketManager.setReplyOpCodeHandler(C4PacketBuilder.GAME_ENDED_BY_CLIENT,
                (data) -> {
                    changeGameState(GameState.GAME_ENDED_MANUALLY);
                });
    }

    /**
     * Sets the callback that executes when the state of the GameState of the game changes.
     * The GameState is passed to the callback.
     * @param onGameStateChange The callback that executes when the state of the GameState of the game changes.
     *                          The GameState is passed to the callback.
     */
    public void setOnGameStateChange(Consumer<GameState> onGameStateChange) {
        this.onGameStateChange = onGameStateChange;
    }

    /**
     * Sets the callback that executes when a move is played by the player.
     * The column and row of the move is passed to the callback.
     * @param onMovePlayed The callback that executes when a move is played by the player.
     *                     The column and row of the move is passed to the callback.
     */
    public void setOnMovePlayed(BiConsumer<Integer, Integer> onMovePlayed) {
        this.onMovePlayed = onMovePlayed;
    }

    /**
     * Sets the callback that executes when the marker count for the client or
     * server changes. Both marker counts are passed to the callback.
     * @param onMarkerCountChange The callback that executes when the marker count for the client or
     *                             server changes. Both marker counts are passed to the callback.
     */
    public void setOnMarkerCountChange(BiConsumer<Integer, Integer> onMarkerCountChange) {
        this.onMarkerCountChange = onMarkerCountChange;
    }

    /**
     * Sets the callback that executes when a column becomes full. The column # is
     * passed to the callback.
     * @param onColumnFull The callback that executes when a column becomes full. The column # is
     *                      passed to the callback.
     */
    public void setOnColumnFull(Consumer<Integer> onColumnFull) {
        this.onColumnFull = onColumnFull;
    }

    /**
     * Sets the callback that executes when there is problem to communicate with the server.
     * @param onCommunicationError The callback that executes when there is problem to communicate with the server.
     */
    public void setOnCommunicationError(Action onCommunicationError) {
        this.onCommunicationError = onCommunicationError;
    }

    /**
     * Returns the current GameState of the game
     * @return The current GameState of the game
     */
    public GameState getGameState() {
        return gameState;
    }
}


