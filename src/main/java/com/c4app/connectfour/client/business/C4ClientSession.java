package com.c4app.connectfour.client.business;

import com.c4app.connectfour.util.Action;
import com.c4app.connectfour.util.C4PacketBuilder;
import com.c4app.connectfour.util.SocketManager;

import java.io.IOException;
import java.net.Socket;
import java.util.function.Consumer;

/**
 * The C4ClientSession is responsible creating C4ClientGames when the user
 * request to do so.
 *
 * To decouple this class from the GUI, the C4ClientSession executes callbacks
 * when different things happen.
 *
 * Description of each callback:
 * 
 *      onGameStarted -> executes when a new game has been started. A C4ClientGame
 *                       is passed to the callback
 *      onSessionEnded -> executes when a the session has ended
 *      onCommunicationError -> executes when there is problem to communicate with the
 *                              server.
 *
 * The callbacks of the C4ClientSession need to be set for it to work properly.
 *
 * @author Yanik
 */
public class C4ClientSession {
    private Consumer<C4ClientGame> onGameStarted;
    private Action onSessionEnded;
    private Action onCommunicationError;

    private Socket socket;
    private SocketManager socketManager;


    public C4ClientSession(Socket socket) {
        this.socket = socket;
        this.socketManager = new SocketManager(socket);

        setupReplyOpCodeActions();
    }

    /**
     * Sends a request to the server to start a new game (REQUEST_GAME_START).
     */
    public void requestStartGame() {
        try {
            byte[] gameStartPacket = C4PacketBuilder.requestGameStart();
            socketManager.sendPacket(gameStartPacket, true);
        } catch (IOException e) {
            onCommunicationError.execute();
        }
    }

    /**
     * Sends a request to the server to end the session (REQUEST_END_SESSION).
     */
    public void requestEndSession() {
        try {
            byte[] gameStartPacket = C4PacketBuilder.requestEndSession();
            socketManager.sendPacket(gameStartPacket, true);
        } catch (IOException e) {
            onCommunicationError.execute();
        }
    }

    /**
     * Starts a new C4ClientGame.
     */
    private void startGame() {
        C4ClientGame c4ClientGame = new C4ClientGame(socket);
        onGameStarted.accept(c4ClientGame);
    }

    /**
     * Ends the current session (closes the socket).
     */
    private void endSession() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                // Normally exception shouldn't be thrown, it only happens on rare occasions
                // that shouldn't affect the functionality of the application
            }
        }

        onSessionEnded.execute();
    }

    /**
     * Sets the callbacks for all the Op Codes that might be received from the server.
     */
    private void setupReplyOpCodeActions() {
        socketManager.setReplyOpCodeHandler(C4PacketBuilder.GAME_STARTED_BY_SERVER,
                (data) -> {
                    startGame();
                });

        socketManager.setReplyOpCodeHandler(C4PacketBuilder.SESSION_WILL_END,
                (data) -> {
                    endSession();
                });
    }

    /**
     * Sets the callback that executes when a new game has been started. A C4ClientGame
     * is passed to the callback.
     * @param onGameStarted The callback that executes when a new game has been started. A C4ClientGame
     *                      is passed to the callback
     */
    public void setOnGameStarted(Consumer<C4ClientGame> onGameStarted) {
        this.onGameStarted = onGameStarted;
    }

    /**
     * Sets the callback that executes when a the session has ended.
     * @param onSessionEnded The callback that executes when a the session has ended
     */
    public void setOnSessionEnded(Action onSessionEnded) {
        this.onSessionEnded = onSessionEnded;
    }

    /**
     * Sets the callback that executes when there is problem to communicate with the server.
     * @param onCommunicationError The callback that executes when there is problem to communicate with the server.
     */
    public void setOnCommunicationError(Action onCommunicationError) {
        this.onCommunicationError = onCommunicationError;
    }
}
