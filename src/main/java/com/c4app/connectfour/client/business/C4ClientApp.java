package com.c4app.connectfour.client.business;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.function.Consumer;

/**
 * The C4ClientApp is responsible for establishing a connection with the server.
 *
 * To decouple this class from the GUI, the C4ClientApp executes callbacks
 * when different things happen.
 *
 * Description of each callback:
 * 
 *      onConnectionError -> executed if there is an error when trying to establish
 *                           connection with the server (the exception is passed to
 *                           the callback so that it will be displayed to the user in a
 *                           user friendly way).
 *      onConnected -> executed when a connection has been established with the
 *                     server. The C4ClientSession is passed to the callback.
 *
 * The callbacks of the C4ClientApp need to be set for it to work properly.
 *
 * @author Yanik
 */
public class C4ClientApp {
    private Consumer<Exception> onConnectionError;
    private Consumer<C4ClientSession> onConnected;

    /**
     * Tries to connect to the specified host and port #.
     *
     * @param host The host to connect to.
     * @param port The port # to use when connecting to the port.
     */
    public void connectToServer(String host, int port) {
        Socket socket = new Socket();

        try {
            socket.connect(new InetSocketAddress(host , port));
            C4ClientSession c4ClientSession = new C4ClientSession(socket);
            onConnected.accept(c4ClientSession);

        } catch (UnknownHostException  uhe) {
            onConnectionError.accept(uhe);
        } catch (IOException ioe) {
            onConnectionError.accept(ioe);
        }
    }

    /**
     * Sets the callback that is executed if there is an error when trying to establish connection
     * with the server (the exception is passed to the callback so that it will be displayed
     * to the user in a user friendly way.
     *
     * It is required to set this callback for the the C4ClientApp to work properly.
     * 
     * @param onConnectionError The callback that is executed if there is an error when trying 
     *                          to establish connection with the server.
     */
    public void setOnConnectionError(Consumer<Exception> onConnectionError) {
        this.onConnectionError = onConnectionError;
    }

    /**
     * Sets the callback that is executed when a connection has been established with the
     * server. The C4ClientSession is passed to the callback.
     *
     * It is required to set this callback for the the C4ClientApp to work properly.
     * 
     * @param onConnected
     */
    public void setOnConnected(Consumer<C4ClientSession> onConnected) {
        this.onConnected = onConnected;
    }
}
