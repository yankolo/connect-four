package com.c4app.connectfour.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.function.Consumer;

/**
 * The SocketManager class encapsulates all the sending/receiving of packets of the Connect Four
 * server and client.
 *
 * This class makes it very easy to send and receive packets, as it is possible to assign callbacks
 * for different op codes that are received. In other words, is it possible to set callbacks that will
 * be executed when specific op codes are received.
 * The SocketManager achieves this by having a HashMap that maps different op codes to different
 * op codes.
 *
 * A callback is represented by a Consumer<Byte> object, which is basically a functional interface (lets us use lambdas)
 * that is defined in the java.util.function package. The Consumer functional interface defines an object
 * that takes an object as input and returns no output. In our case, the input is a Byte object (byte wrapper)
 * which is the data that is in the packet received (reminder, a C4 packet consists of two bytes: the op code and the data).
 * In short, when a specific op code is received, the associated callback in the HashMap is executed with the
 * packet data as the callback argument.

 * To register callbacks for specific received op codes use the setReplyOpCodeHandler(byte, Consumer<Byte>)
 * method.
 *
 * @author Yanik
 */
public class SocketManager {
    private Socket socket;
    private HashMap<Byte, Consumer<Byte>> opCodeHandlers;

    public SocketManager(Socket socket) {
        this.socket = socket;
        this.opCodeHandlers = new HashMap<>();
    }

    /**
     * Assigns a callback to the specified reply op code.
     * In other words, when the packet is received with this op code, the assigned callback is
     * executed.
     *
     * @param opCode The op code to assign the callback to
     * @param opCodeHandler The callback to execute when the op code is received
     */
    public void setReplyOpCodeHandler(byte opCode, Consumer<Byte> opCodeHandler) {
        opCodeHandlers.put(opCode, opCodeHandler);
    }

    /**
     * Removes the callback that was assigned to the specified reply op code.
     *
     * @param opCode The op code that the callback was assigned to
     */
    public void removeReplyOpCodeHandler(byte opCode) {
        opCodeHandlers.remove(opCode);
    }

    /**
     * Sends a packet to the other end of the socket.
     * If a reply is expected, it is necessary to set the "replyExpected" parameter to true.
     * By setting the "replyExpected" to true, the method will wait for a reply from the other end of the
     * socket, after which it will execute the callback that was assigned to the received op code.
     *
     * @param packet The packet to send
     * @param replyExpected If true the method will wait for a reply from the other end of the socket
     *                      and execute the callback that was assigned to the received op code.
     * @throws IOException An exception is thrown if something happens when trying to communicate with the other end
     *                      of the socket. Might be one the following: The socket has closed or there is not internet connection
     *                      (or any other communication error)
     */
    public void sendPacket(byte[] packet, boolean replyExpected) throws IOException {
        OutputStream out = socket.getOutputStream();
        out.write(packet);

        if (replyExpected) {
             waitForReply();
        }
    }
    
    /**
     * This method waits until it received a packet. After receiving the packet it will execute
     * the callback that is associated with the op code in this packet
     */
    public void waitForReply() throws IOException {
        byte[] replyPacket = getReply();
        executePacketHandler(replyPacket[0], replyPacket[1]);
    }

    /**
     * Waits for a reply from the other end of the socket.
     * Expects a packet of 2 bytes.
     *
     * @return The packet received
     */
    private byte[] getReply() throws IOException {
        try {
            InputStream in = socket.getInputStream();

            byte[] byteBuffer = new byte[2];
            int totalBytesReceived = 0;
            int bytesReceived;

            while (totalBytesReceived < byteBuffer.length) {
                bytesReceived = in.read(byteBuffer, totalBytesReceived, byteBuffer.length - totalBytesReceived);

                if (bytesReceived == -1) {
                    throw new RuntimeException("Unexpected end of input stream has been reached");
                } else {
                    totalBytesReceived += bytesReceived;
                }
            }

            return byteBuffer;
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    /**
     * Executes the callback that is assigned to the specified op code.
     * If no callback is assigned to the op code, nothing happens.
     *
     * @param opCode An op code that has an callback assigned to it.
     */
    private void executePacketHandler(byte opCode, byte data) {
        Consumer<Byte> opCodeHandler = opCodeHandlers.get(opCode);

        if (opCodeHandler != null)
            opCodeHandlers.get(opCode).accept(data);
    }
}
