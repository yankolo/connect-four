package com.c4app.connectfour.util;

/**
 * Represents an operation that accepts no input and returns no
 * result. This functional interface is useful to set callbacks using lambdas
 * that do not require any input and do not return anything.
 *
 * java.util.function provides many functional interfaces, however
 * there is no functional interface that accepts no input and returns no
 * result.
 *
 * @author Yanik
 */
@FunctionalInterface
public interface Action {
    /**
     * Performs this action
     */
    void execute();
}
