package com.c4app.connectfour.util;

/**
 * The C4PacketBuilder centralizes the creation of all the packets in the Connect Four
 * application.
 * It contains static methods that makes it easier to create the byte arrays
 * that are sent by the client or the server.
 *
 * It also contains all the possible op codes as public static final fields.
 *
 * It is useful to centralize the creation of packets for many reasons. For example,
 * if it is needed to change an op code, it is not needed to change it everywhere it is used,
 * instead it is only needed to change it in the C4PacketBuilder.
 * 
 * @author Thomas
 *
 */
public class C4PacketBuilder {

    // --- Op Codes used by the client ---

    public static final byte REQUEST_GAME_START = 0x01;
    public static final byte CLIENT_PLAY_MOVE = 0x02;
    public static final byte REQUEST_END_GAME = 0x03;
    public static final byte REQUEST_END_SESSION = 0X04;

    // --- Op Codes used by the server ---

    public static final byte GAME_STARTED_BY_SERVER = 0x11;
    public static final byte SERVER_MOVED = 0x12;
    public static final byte CLIENT_WON_GAME = 0x13;
    public static final byte SERVER_MOVED_CLIENT_LOST = 0x14;
    public static final byte SERVER_MOVED_GAME_TIE = 0x15;
    public static final byte SESSION_WILL_END = 0x16;
    public static final byte GAME_ENDED_BY_CLIENT = 0x17;

    // --- Packets sent by the client ---
	
	public static byte[] requestGameStart() {
		return new byte[] { REQUEST_GAME_START, -1 };
	}

	public static byte[] clientPlayMove(int col) {
		return new byte[] { CLIENT_PLAY_MOVE, (byte) col };
	}

	public static byte[] requestEndGame() {
		return new byte[] { REQUEST_END_GAME, -1 };
	}
	
	public static byte[] requestEndSession() {
		return new byte[] { REQUEST_END_SESSION, -1 };
	}

    // --- Packets sent by the server ---
	
	public static byte[] gameStartedByServer() {
		return new byte[] { GAME_STARTED_BY_SERVER, -1 };
	}

	public static byte[] serverMoved(int col) {
		return new byte[] { SERVER_MOVED, (byte) col };
	}

	public static byte[] clientWonGame() {
		return new byte[] { CLIENT_WON_GAME, -1 };
	}

	public static byte[] serverMovedClientLost(int col) {
		return new byte[] { SERVER_MOVED_CLIENT_LOST, (byte) col };
	}

	public static byte[] serverMovedGameTie(int col) {
		return new byte[] { SERVER_MOVED_GAME_TIE, (byte) col };
	}

	public static byte[] sessionWillEnd() {
		return new byte[] { SESSION_WILL_END, -1 };
	}

	public static byte[] gameEndedByClient() {
		return new byte[] { GAME_ENDED_BY_CLIENT, -1 };
	}

}
